<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.4.9
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2009 Invision Power Services
|
|   ========================================
|
|
|
+---------------------------------------------------------------------------
*/

$PRE = trim(ipsRegistry::dbFunctions()->getPrefix());
$DB  = ipsRegistry::DB();

/* warn logs table */

$TABLE	= 'warn_logs';
$SQL[]	= "ALTER TABLE warn_logs ADD INDEX ( wlog_mid, wlog_date );";


