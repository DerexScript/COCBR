<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.4.9
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2004 Invision Power Services
|
|   ========================================
|
|
|
+---------------------------------------------------------------------------
*/


# Nothing of interest!

$SQL[] = "ALTER TABLE sessions CHANGE browser browser VARCHAR(200) NOT NULL default '';";
$SQL[] = "ALTER TABLE rss_import ADD rss_import_allow_html TINYINT(1) NOT NULL default '0';";

