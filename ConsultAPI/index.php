<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<base href="/" />
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<title>Clash Of Clans Dicas, Consulte API</title>
	<script src="https://use.fontawesome.com/870060c86f.js"></script>
	<link rel="icon" href="../img/ico.png" />
	<meta property="og:locale" content="pt-BR">
	<meta property="og:title" content="COCBR">
	<meta property="og:description" content="Clash Of Clans & Clash Of Royale Dicas, Consultas de informações sobre o jogo em tempo real,  Noticias, Layouts, et etc..">
	<meta property="og:image" content="../img/coclogo.png"/>
	<style type="text/css">
		.d-block {
			max-height: 400px !important;
		}

		@font-face {
			font-family: scfont;
			src: url('../font/Supercell-magic-webfont.ttf');
		}

		.tb11 {
			background:#FFFFFF url("../img/cerquilha.ico") no-repeat 4px 8px;
			padding:4px 4px 4px 22px;
			border:1px solid #CCCCCC;
		}
	</style>
	
	<script src="../js/myFunction.js"></script>
</head>
<body>
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100" src="../img/slide1.png" alt="First slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="../img/slide2.jpg" alt="Second slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="../img/slide3.jpg" alt="Third slide">
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>


	<nav class="navbar navbar-expand navbar-light" style="background-color: #E6F48A;">
		<img src="../img/coclogo.png" style="max-height: 50px;">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="//<?php echo $_SERVER['HTTP_HOST'];?>">Home</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="../ConsultAPI/">Consult API COC<span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="forum/">Forum</a>
				</li>
			</ul>
		</div>
	</nav>

	<?php
	//echo $_SERVER['HTTP_USER_AGENT']."<br />";
	?>
	<div class="container-fluid" style="margin-top: 5px;">
		<div class="row">

			<div class="mx-auto"><h4 id="PlayerConsult" style="font:20px scfont, Arial, Tahoma, Sans-serif;">Consultar API</h4></div>
		</div>
		
		

		

		<div class="row">
			<nav class="nav flex-column col-2">
				<span class="nav-link" style="font:20px scfont, Arial, Tahoma, Sans-serif;">Navegação</span>
				<a class="nav-link" href="//<?php echo $_SERVER['HTTP_HOST'];?>">Home</a>
				<a class="nav-link active" href="ConsultAPI/">Consult API COC</a>
				<a class="nav-link" href="forum/">Forum</a>
			</nav>
			<form class="mx-auto form-inline" id="formSearch" onsubmit="return fn1(event);">
				<div class="form-group">
					<label for="SelectOptions" class="sr-only">Selecione uma opção</label>
					<select class="form-control" id="SelectOptions" name="searchType" onchange="selectChange(this.value);" required>
						<option value="#" selected="">Selecione</option>
						<optgroup label="Clans">
							<option value="SearchClans">Pesquisar Clã</option>
							<option value="GetClanInformation">Obter Informações Do Clã</option>
							<option value="ListClanMembers">Listar membros do clã</option>
							<option value="Retrieveclansclanwarlog">Log De Guerra</option>
							<option value="Retrieveinformationaboutclanscurrentclanwar">Log De Guerra Atual</option>
						</optgroup>
						<optgroup label="Locations">
							<option value="ListLocations">Lista de Licalizações</option>
							<option value="Getlocationinformation">Obter Informações Do Local</option>
							<option value="Getclanrankingsforaspecificlocation">Rank De Clã Por Localização</option>
							<option value="Getplayerrankingsforaspecificlocation">Rank De Jogadores Por Localização</option>
							<option value="Getclanversusrankingsforaspecificlocation">Rank De Guerras De Clã Por Localização</option>
							<option value="Getplayerversusrankingsforaspecificlocation">Rank De Guerras De Jogadores Por Localização</option>
						</optgroup>
						<optgroup label="Players">
							<option value="Getplayerinformation">Obter Informações De Jogadores</option>
						</optgroup>
					</select>
				</div>
				<div class="form-group mx-sm-1">
					<label for="inputClanTag" class="sr-only">Password</label>
					<input type="password" class="form-control" id="inputClanTag" name="inputClanTag" placeholder="Selecione alguma opção" required disabled>
				</div>
				<button type="submit" class="btn btn-outline-success mx-sm-1">Pesquisar</button>
				<button type="button" class="btn btn-outline-danger mx-sm-1" onclick="return clearResults(event);">Limpar</button>
			</form>
		</div>
		<div id="clanInformations2"></div>
	</div>
	<footer class="bg-dark mt-4">
		<div class="container-fluid py-3" style="background-color: #E6F48A;">
			<div class="row">
				<div class="col-md-3">
					<h5>Clash Of Clans & Clash Of Royale</h5></div>
					<div class="col-md-3"></div>
					<div class="col-md-3"></div>
					<div class="col-md-3"></div>
				</div>
				<div class="row">
					<div class="col-md-6"><a href="https://pt.wikipedia.org/wiki/Clash_of_Clans" target="_blank">Clash of Clans - Wikipedia</a><span class="small"><br>&copy; Copyright 2017 - <?php echo date("Y"); ?>, Comunidade "Clash Of Clans" e "Clash Of Royale" Brasil - Todos direitos reservados.</span></div>
					<div class="col-md-3"></div>
					<div class="col-md-3 text-right small align-self-end">Project: <a href="https://github.com/DerexScript/COCBR" target="_blank" class="fa fa-github" aria-hidden="true"> GitHub</a>, <a href="https://gitlab.com/DerexScript/" target="_blank" class="fa fa-gitlab" aria-hidden="true"> GitLab</a></div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="../js/bootstrap.min.js"></script>
</body>
</html>