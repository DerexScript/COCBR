<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.4.9
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2009 Invision Power Services
|
|   ========================================
|
|
|
+---------------------------------------------------------------------------
*/

$PRE = trim(ipsRegistry::dbFunctions()->getPrefix());
$DB  = ipsRegistry::DB();

$TABLE	= 'admin_login_logs';
$SQL[]	= "ALTER TABLE admin_login_logs CHANGE admin_ip_address admin_ip_address VARCHAR( 46 ) NOT NULL DEFAULT '0.0.0.0';";


