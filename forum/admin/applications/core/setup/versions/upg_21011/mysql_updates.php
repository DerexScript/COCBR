<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.4.9
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2004 Invision Power Services
|
|   ========================================
|
|
|
+---------------------------------------------------------------------------
*/


$SQL[] = "ALTER TABLE members CHANGE email email varchar( 150 ) NOT NULL default ''";

$SQL[] = "ALTER TABLE subscription_currency CHANGE `subcurrency_exchange` `subcurrency_exchange` DECIMAL( 16, 8 ) DEFAULT '0.00000000' NOT NULL";

