<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.4.9
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2009 Invision Power Services
|
|   ========================================
|
|
|
+---------------------------------------------------------------------------
*/

$PRE = trim(ipsRegistry::dbFunctions()->getPrefix());
$DB  = ipsRegistry::DB();

$TABLE	= 'dnames_change';
$SQL[]	= "ALTER TABLE dnames_change CHANGE dname_ip_address dname_ip_address VARCHAR( 46 ) NOT NULL;";


