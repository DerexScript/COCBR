<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.4.9
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2009 Invision Power Services
|
|   ========================================
|
|
|
+---------------------------------------------------------------------------
*/

$PRE = trim(ipsRegistry::dbFunctions()->getPrefix());
$DB  = ipsRegistry::DB();


$TABLE	= 'error_logs';
$SQL[]	= "ALTER TABLE error_logs CHANGE log_ip_address log_ip_address VARCHAR( 46 ) NULL DEFAULT NULL, 
	CHANGE log_date log_date INT( 10 ) NOT NULL DEFAULT '0';";


