	function clearResults(event) {
		document.getElementById('clanInformations2').innerHTML = '';
		document.getElementById('inputClanTag').value = '';
		return false;
	}

	function selectChange(valueSelect) {
		if (valueSelect == "SearchClans") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.remove("tb11");
			//document.getElementById('changeIndication').innerText = "Clan Name";
			document.getElementById('inputClanTag').setAttribute("placeholder", "Clan Name");
		} else if (valueSelect == "GetClanInformation") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.add("tb11");
			//document.getElementById('changeIndication').innerText = "#";
			document.getElementById('inputClanTag').setAttribute("placeholder", "Clan Tag");
		} else if (valueSelect == "ListClanMembers") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.add("tb11");
			//document.getElementById('changeIndication').innerText = "#";
			document.getElementById('inputClanTag').setAttribute("placeholder", "Clan Tag");
		} else if (valueSelect == "Retrieveclansclanwarlog") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			document.getElementById('inputClanTag').setAttribute("type", "text");
			//document.getElementById('changeIndication').innerText = "#";
			document.getElementById("inputClanTag").classList.add("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Clan Tag");
		} else if (valueSelect == "Retrieveinformationaboutclanscurrentclanwar") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			document.getElementById('inputClanTag').setAttribute("type", "text");
			//document.getElementById('changeIndication').innerText = "#";
			document.getElementById("inputClanTag").classList.add("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Clan Tag");
		} else if (valueSelect == "ListLocations") {
			//document.getElementById('changeIndication').innerText = "";
			//document.getElementById('inputClanTag').setAttribute("type", "hidden");
			document.getElementById('inputClanTag').setAttribute("placeholder", "List Locations");
			document.getElementById("inputClanTag").classList.remove("tb11");
			document.getElementById('inputClanTag').setAttribute("disabled", "disabled");
		} else if (valueSelect == "Getlocationinformation") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			//document.getElementById('changeIndication').innerText = "ID";
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.remove("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Localização ID");
		} else if (valueSelect == "Getclanrankingsforaspecificlocation") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			//document.getElementById('changeIndication').innerText = "ID";
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.remove("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Localização ID");
		} else if (valueSelect == "Getplayerrankingsforaspecificlocation") {
			//document.getElementById('changeIndication').innerText = "ID";
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.remove("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Localização ID");
		} else if (valueSelect == "Getclanversusrankingsforaspecificlocation") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			//document.getElementById('changeIndication').innerText = "ID";
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.remove("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Localização ID");
		} else if (valueSelect == "Getplayerversusrankingsforaspecificlocation") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			//document.getElementById('changeIndication').innerText = "ID";
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.remove("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Localização ID");
		}else if (valueSelect == "Getplayerinformation") {
			document.getElementById('inputClanTag').removeAttribute("disabled", "disabled");
			//document.getElementById('changeIndication').innerText = "#";
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.add("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Player Tag");
		}else if (valueSelect == "#") {
			document.getElementById('inputClanTag').setAttribute("disabled", "disabled");
			//document.getElementById('changeIndication').innerText = "";
			document.getElementById('inputClanTag').setAttribute("type", "text");
			document.getElementById("inputClanTag").classList.remove("tb11");
			document.getElementById('inputClanTag').setAttribute("placeholder", "Selecione alguma opção");
		}
	}

	function getClan1(clanTag, optSearch, callback){
		var cosProxy = "https://cors-anywhere.herokuapp.com/";
		var url = "../API/getClan.php";
		var params = "clanTag=" + clanTag + "&optSearch=" + optSearch;
		var client;
		if (window.XMLHttpRequest){
			client = new XMLHttpRequest()
		}else if(window.ActiveXObject){
			try {
				client = new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e){
				try {
					client = new ActiveXObject("Microsoft.XMLHTTP");
				}catch(e){
					alert("AJAX não suportado!");
					return false;
				}
			}
		}
		client.open("POST", url, true);
		client.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		client.setRequestHeader('Content-Type', 'application/json')
		client.onreadystatechange = function(){
			if (client.readyState == 4 && client.status == 200) {
				callback(JSON.parse(client.responseText));
			}
		}
		client.send(params);
	}

	function recordSearch(optSearch='', valueSearch=''){
		var url1 = "../API/recordSearch.php";
		var params1 = "optSearch=" + optSearch+ "&valueSearch="+valueSearch;
		var client1;
		if (window.XMLHttpRequest){
			client1 = new XMLHttpRequest()
		}else if(window.ActiveXObject){
			try {
				client1 = new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e){
				try {
					client1 = new ActiveXObject("Microsoft.XMLHTTP");
				}catch(e){
					alert("AJAX não suportado!");
					return false;
				}
			}
		}
		client1.open("POST", url1, true);
		client1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		client1.send(params1);
	}

	function fn1(){
		var inputClanTag = document.getElementById("inputClanTag").value;
		var stringinputClanTag;
		var count = 0;
		if (inputClanTag.indexOf("#") !== -1) {
			stringinputClanTag = inputClanTag.substr(1, inputClanTag.length - 1);
		} else {
			stringinputClanTag = document.getElementById("inputClanTag").value;
		}
		getClan1(stringinputClanTag, document.getElementById('SelectOptions').value, function(resp){
			if (document.getElementById('SelectOptions').value == "ListClanMembers") {
				var strHtml = '<div class="jumbotron row justify-content-center align-self-center">'+
				'<h1>Clan #'+stringinputClanTag+'</h1>'+
				'</div>'+
				'<div class="row">'+
				'<div class="col-12">'+
				'<div style="position: relative; overflow: auto; width: 100%;">'+
				'<table class="table table-striped">'+
				'<thead>'+
				'<tr>'+
				'<th>Rank</th>'+
				'<th>Tag</th>'+
				'<th>Nome</th>'+
				'<th>Cargo</th>'+
				'<th>XP</th>'+
				'<th>Clã ID</th>'+
				'<th>Liga</th>'+
				'<th>Troféus</th>'+
				'<th>Troféus Casa Do Construtor</th>'+
				'<th>Posição Anterior No Clã</th>'+
				'<th>Tropas Doadas</th>'+
				'<th>Tropas Recebidas</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'

				for (var i in resp.items) {
					count++;
					strHtml += '<tr>'+
					'<th scope="row">'+count+'</th>'+
					'<td>'+resp.items[i].tag+'</td>'+
					'<td>'+resp.items[i].name+'</td>'+
					'<td>'+resp.items[i].role+'</td>'+
					'<td>'+resp.items[i].expLevel+'</td>'+
					'<td>'+resp.items[i].league.id+'</td>'+
					'<td>'+resp.items[i].league.name+' <img src="'+resp.items[i].league.iconUrls.small+'"></td>'+
					'<td>'+resp.items[i].trophies+'</td>'+
					'<td>'+resp.items[i].versusTrophies+'</td>'+
					'<td>'+resp.items[i].previousClanRank+'</td>'+
					'<td>'+resp.items[i].donations+'</td>'+
					'<td>'+resp.items[i].donationsReceived+'</td>'+
					'</tr>'
				}
				strHtml += '</tbody>'+
				'</table>'+
				'</div>'+
				'</div>'
				recordSearch('ListClanMembers', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			} else if (document.getElementById('SelectOptions').value == "SearchClans") {
				var strHtml = '<div class="jumbotron row justify-content-center align-self-center">'+
				'<h1>'+document.getElementById("inputClanTag").value+'</h1>'+
				'</div>'+
				'<center ><h2> << Arraste para os lados para ver mais informações >> </h2></center>'+
				'<div class="row">'+
				'<div class="col-12">'+
				'<!-- style="table-layout: fixed; word-wrap: break-word;" -->'+
				'<div style="position: relative; overflow: auto; width: 100%; height: 500px;">'+
				'<table class="table table-hover table-inverse table-responsive">'+
				'<thead>'+
				'<tr>'+
				'<th>Lista</th>'+
				'<th>Tag</th>'+
				'<th>Nome do Clã</th>'+
				'<th>Recrutamento</th>'+
				'<th>ID Localização</th>'+
				'<th>Pais</th>'+
				'<th>Brasileiro</th>'+
				'<th>Código Do País</th>'+
				'<th>Nível</th>'+
				'<th>Pontos Do Clã</th>'+
				'<th>Troféus Necessários CC</th>'+
				'<th>Troféus Necessários CV</th>'+
				'<th>Guerras</th>'+
				'<th>Batalhas Ganhas a risca</th>'+
				'<th>Guerras Ganhas</th>'+
				'<th>Laços De Guerra</th>'+
				'<th>Guerras Perdidas</th>'+
				'<th>Guerras Publica</th>'+
				'<th>Quantidade De Membros</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'
				for (var i in resp.items) {
					count++;
					if (resp.items[i].type === "closed") {
						resp.items[i].type = "Fechado"
					} else if (resp.items[i].type === "inviteOnly") {
						resp.items[i].type = "Apenas Convidado"
					} else if (resp.items[i].type === "open") {
						resp.items[i].type = "Aberto"
					}
					if (resp.items[i].warFrequency === "moreThanOncePerWeek") {
						resp.items[i].warFrequency = "Mais De Uma Vez Por Semana";
					} else if (resp.items[i].warFrequency === "always") {
						resp.items[i].warFrequency = "Sempre";
					} else if (resp.items[i].warFrequency === "lessThanOncePerWeek") {
						resp.items[i].warFrequency = "Menos De Uma Vez Por Semana";
					} else if (resp.items[i].warFrequency === "never") {
						resp.items[i].warFrequency = "Nunca";
					} else if (resp.items[i].warFrequency === "oncePerWeek") {
						resp.items[i].warFrequency = "Uma Vez Por Semana";
					}
					strHtml += '<tr>'+
					'<th scope="row">'+parseInt(count)+'</th>'+
					'<td>'+resp.items[i].tag+'</td>'+
					'<td>'+resp.items[i].name+' <img src="'+resp.items[i].badgeUrls.small+'"/></td>'+
					'<td>'+resp.items[i].type+'</td>'
					try {
						strHtml += 
						'<td>'+resp.items[i].location.id+'</td>'+
						'<td>'+resp.items[i].location.name+'</td>'+
						'<td>'+resp.items[i].location.isCountry+'</td>'+
						'<td>'+resp.items[i].location.countryCode+'</td>'
					} catch (e) {
						strHtml += 
						'<td>null</td>'+
						'<td>null</td>'+
						'<td>null</td>'+
						'<td>null</td>'
					}
					strHtml += 
					'<td>'+resp.items[i].clanLevel+'</td>'+
					'<td>'+resp.items[i].clanPoints+'</td>'+
					'<td>'+resp.items[i].clanVersusPoints+'</td>'+
					'<td>'+resp.items[i].requiredTrophies+'</td>'+
					'<td>'+resp.items[i].warFrequency+'</td>'+
					'<td>'+resp.items[i].warWinStreak+'</td>'+
					'<td>'+resp.items[i].warWins+'</td>'+
					'<td>'+resp.items[i].warTies+'</td>'+
					'<td>'+resp.items[i].warLosses+'</td>'+
					'<td>'+resp.items[i].isWarLogPublic+'</td>'+
					'<td>'+resp.items[i].members+'</td>'+
					'</tr>'
				}
				strHtml += '</tbody>'+
				'</table>'+
				'</div>';
				recordSearch('SearchClans', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			} else if (document.getElementById('SelectOptions').value == "GetClanInformation") {
				if (resp.warFrequency === 'moreThanOncePerWeek') {
					resp.warFrequency = 'Mais de uma vez por semana';
				}
				if (resp.type === 'inviteOnly') {
					resp.type = 'Somente por Convite';
				}
				var strHtml = '<div class="jumbotron row justify-content-center align-self-center">'+
				'<h1>Clan #'+stringinputClanTag+'</h1>'+
				'</div>'+
				'<div class="row">'+
				'<div class="col-12">'+
				'<center>'+
				'<h2>'+resp.name+'</h2>'+
				'</center>'+
				'</div>'+
				'</div>'+
				'<div class="row">'+
				'<div class="col-12">'+
				'<center>'+
				'<blockquote class="blockquote"><p class="mb-0">'+resp.description+'</p></blockquote>'+
				'</center>'+
				'</div>'+
				'</div>'+
				'<div class="row">'+
				'<div class="col-6">'+
				'<div>'+
				'<center>'+
				'<h2>'+resp.name+'</h2>'+
				'</center>'+
				'</div>'+
				'<div>'+
				'<center>'+
				'<img src="'+resp.badgeUrls.medium+'" alt="LogoClan" />'+
				'</center>'+
				'</div>'+
				'<div>'+
				'<center>'+
				'<b>Nível</b> '+resp.clanLevel+
				'</center>'+
				'</div>'+
				'</div>'+
				'<div class="col-6">'
				strHtml += '<ul class="list-group">'+
				'<li class="list-group-item"><b>Tag:</b> '+resp.tag+'</li>'+
				'<li class="list-group-item"><b>Nome:</b> '+resp.name+'</li>'+
				'<li class="list-group-item"><b>Acesso:</b> '+resp.type+'</li>'+
				'<li class="list-group-item"><b>País ID:</b> '+resp.location.id+'</li>'+
				'<li class="list-group-item"><b>País:</b> '+resp.location.name+'</li>'+
				'<li class="list-group-item"><b>Código do país:</b> '+resp.location.countryCode+'</b></li>'+
				'<li class="list-group-item"><b>Pontos do clan:</b> '+resp.clanPoints+'</li>'+
				'<li class="list-group-item"><b>ClanVersusPoints:</b> '+resp.clanVersusPoints+'</li>'+
				'<li class="list-group-item"><b>Troféus Necessários:</b> '+resp.requiredTrophies+'</li>'+
				'<li class="list-group-item"><b>Frequência de guerra:</b> '+resp.warFrequency+'</li>'+
				'<li class="list-group-item"><b>Batalhas Ganhas a risca:</b> '+resp.warWinStreak+'</li>'+
				'<li class="list-group-item"><b>Batalhas Ganhas:</b> '+resp.warWins+'</li>'+
				'<li class="list-group-item"><b>Batalhas publica:</b> '+resp.isWarLogPublic+'</li>'+
				'<li class="list-group-item"><b>Quantidade de membros:</b> '+resp.members+'</li>'+
				'</ul>'+
				'</div>'+
				'<div class="col-12">'+
				'<div style="position: relative; overflow: auto; width: 100%;">'+
				'<table class="table table-hover table-inverse">'+
				'<thead>'+
				'<tr>'+
				'<th>Rank</th>'+
				'<th>Tag</th>'+
				'<th>Nome</th>'+
				'<th>Cargo</th>'+
				'<th>XP</th>'+
				'<th>Liga ID</th>'+
				'<th>Nome Da Liga</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'
				for (var i in resp.memberList) {
					count++;
					strHtml += '<tr>'+
					'<th scope="row">'+count+'</th>'+
					'<td>'+resp.memberList[i].tag+'</td>'+
					'<td>'+resp.memberList[i].name+'</td>'+
					'<td>'+resp.memberList[i].role+'</td>'+
					'<td>'+resp.memberList[i].expLevel+'</td>'+
					'<td>'+resp.memberList[i].league.id+'</td>'+
					'<td>'+resp.memberList[i].league.name+' <img src="'+resp.memberList[i].league.iconUrls.small+'" alt="Liga" /></td>'+
					'</tr>'
				}
				strHtml += '</tbody>'+
				'</table>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'
				recordSearch('GetClanInformation', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			} else if (document.getElementById('SelectOptions').value == "Retrieveinformationaboutclanscurrentclanwar") {
				var strHtml = '<div class="jumbotron row justify-content-center align-self-center">'+
				'<h1>Clan #'+stringinputClanTag+'</h1>'+
				'</div>'+
				'<div class="row">'+
				'<div class="col-sm-6">'+
				'<div class="card">'+
				'<div class="card-body">'+
				'<div class="d-flex justify-content-end">'+
				'<div class="mr-auto p-2">'+
				'<p class="card-text"><b>Clan Level:</b> '+resp.clan.clanLevel+'<img src="'+resp.clan.badgeUrls.small+'"/></p>'+
				'<p class="card-text"><b>Quantia de ataques:</b> '+resp.clan.attacks+'</p>'+
				'<p class="card-text"><b>Quantia de Estrelas:</b> '+resp.clan.stars+'</p>'+
				'<p class="card-text"><b>Percentual de Destruição:</b> '+resp.clan.destructionPercentage+'</p>'+
				'<p class="card-text"><b>Estado:</b> '+resp.state+'</p>'+
				'</div>'+
				'<div class="p-2">'+
				'<p class="card-text"><b>Clan Level (Oponente):</b> '+resp.opponent.clanLevel+' <img src="'+ resp.opponent.badgeUrls.small+'"/></p>'+
				'<p class="card-text"><b>Quantia de ataques (Oponente):</b> '+resp.opponent.attacks+'</p>'+
				'<p class="card-text"><b>Quantia de Estrelas (Oponente):</b> '+resp.opponent.stars+'</p>'+
				'<p class="card-text"><b>Percentual de Destruição (Oponente):</b> '+resp.opponent.destructionPercentage+'</p>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'
				recordSearch('Retrieveinformationaboutclanscurrentclanwar', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			} else if (document.getElementById('SelectOptions').value == "ListLocations") {
				var strHtml = '<div class="col-12">'+
				'<div style="position: relative; overflow: auto; width: 100%;">'+
				'<table class="table">'+
				'<thead>'+
				'<tr>'+
				'<th>Lista</th>'+
				'<th>ID</th>'+
				'<th>Nome</th>'+
				'<th>É País</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'
				for (var i in resp.items) {
					count++;
					strHtml += '<tr>'+
					'<th scope="row">'+count+'</th>'+
					'<td>'+resp.items[i].id+'</td>'+
					'<td>'+resp.items[i].name+'</td>'+
					'<td>'+resp.items[i].isCountry+'</td>'+
					'</tr>'
				}
				strHtml += '<tbody>'+
				'</table>'+
				'</div>'+
				'</div>'
				recordSearch('ListLocations', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			}else if (document.getElementById('SelectOptions').value == "Getlocationinformation") {
				var strHtml = '<div class="col-12">'+
				'<div style="position: relative; overflow: auto; width: 100%;">'+
				'<table class="table">'+
				'<thead>'+
				'<tr>'+
				'<th>ID</th>'+
				'<th>Nome</th>'+
				'<th>É País</th>'+
				'<th>Código Do País</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'+
				'<tr>'+
				'<th scope="row">'+resp.id+'</th>'+
				'<td>'+resp.name+'</td>'+
				'<td>'+resp.isCountry+'</td>'+
				'<td>'+resp.countryCode+'</td>'+
				'</tr>'+
				'</tbody>'+
				'</table>'+
				'</div>'+
				'</div>'
				recordSearch('Getlocationinformation', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			} else if (document.getElementById('SelectOptions').value == "Getclanrankingsforaspecificlocation") {
				var strHtml = '<div class="col-12">'+
				'<div style="position: relative; overflow: auto; width: 100%;">'+
				'<table class="table">'+
				'<thead>'+
				'<tr>'+
				'<th>Rank</th>'+
				'<th>Tag</th>'+
				'<th>Nome</th>'+
				'<th>Localização ID</th>'+
				'<th>País</th>'+
				'<th>É País</th>'+
				'<th>Código do país</th>'+
				'<th>Nível do clã</th>'+
				'<th>Membros</th>'+
				'<th>Pontos do clã</th>'+
				'<th>Posição anterior</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>';
				for (var i in resp.items) {
					count++;
					strHtml += '<tr>'+
					'<th scope="row">'+count+'</th>'+
					'<td>'+esp.items[i].tag+'</td>'+
					'<td><img src="'+resp.items[i].badgeUrls.small+'" alt="ClãLogo" width="42" /> '+resp.items[i].name+'</td>'+
					'<td>'+resp.items[i].location.id+'</td>'+
					'<td>'+resp.items[i].location.name+'</td>'+
					'<td>'+resp.items[i].location.isCountry+'</td>'+
					'<td>'+resp.items[i].location.countryCode+'</td>'+
					'<td>'+resp.items[i].clanLevel+'</td>'+
					'<td>'+resp.items[i].members+'</td>'+
					'<td>'+resp.items[i].clanPoints+'</td>'+
					'<td>'+resp.items[i].previousRank+'</td>'+
					'</tr>';

				}
				strHtml += '</tbody>'+
				'</table>'+
				'</div>'+
				'</div>'
				recordSearch('Getclanrankingsforaspecificlocation', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			} else if (document.getElementById('SelectOptions').value == "Getplayerrankingsforaspecificlocation") {
				var strHtml = '<div class="col-12">'+
				'<div style="position: relative; overflow: auto; width: 100%;">'+
				'<table class="table table-responsive">'+
				'<thead>'+
				'<tr>'+
				'<th>Rank</th>'+
				'<th>Tag</th>'+
				'<th>Nome</th>'+
				'<th>Nível</th>'+
				'<th>Troféus</th>'+
				'<th>Ataques Ganhos</th>'+
				'<th>Defesas</th>'+
				'<th>Rank Anterior</th>'+
				'<th>Clã Tag</th>'+
				'<th>Nome Do Clã</th>'+
				'<th>Liga ID</th>'+
				'<th>Nome Da Liga</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'
				for (var i in resp.items) {
					count++;
					strHtml += '<tr>'+
					'<th scope="row">'+count+'</th>'+
					'<td>'+resp.items[i].tag+'</td>'+
					'<td>'+resp.items[i].name+'</td>'+
					'<td>'+resp.items[i].expLevel+'</td>'+
					'<td>'+resp.items[i].trophies+'</td>'+
					'<td>'+resp.items[i].attackWins+'</td>'+
					'<td>'+resp.items[i].defenseWins+'</td>'+
					'<td>'+resp.items[i].previousRank+'</td>'
					try {
						strHtml += '<td>'+resp.items[i].clan.tag+'</td>'+
						'<td><img src="'+resp.items[i].clan.badgeUrls.small+'" alt="ClãLogo" width="16" /> '+resp.items[i].clan.name+'</td>'
					} catch (e) {
						strHtml += '<td>Sem Clã</td>'+
						'<td>Sem Clã</td>'
					}
					strHtml += '<td>'+resp.items[i].league.id+'</td>'+
					'<td><img src="'+resp.items[i].league.iconUrls.small+'" alt="ClãLogo" width="16" /> '+resp.items[i].league.name+'</td>'+
					'</tr>'
				}
				strHtml += '</tbody>'+
				'</table>'+
				'</div>'+
				'</div>'
				recordSearch('Getplayerrankingsforaspecificlocation', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			} else if (document.getElementById('SelectOptions').value == "Getclanversusrankingsforaspecificlocation") {
				var strHtml = '<div class="col-12">'+
				'<div style="position: relative; overflow: auto; width: 100%;">'+
				'<table class="table table-responsive">'+
				'<thead>'+
				'<tr>'+
				'<th>Rank</th>'+
				'<th>Tag</th>'+
				'<th>Nome</th>'+
				'<th>Localização ID</th>'+
				'<th>País</th>'+
				'<th>É País</th>'+
				'<th>Código do país</th>'+
				'<th>Nível do clã</th>'+
				'<th>Membros</th>'+
				'<th>Posição anterior</th>'+
				'<th>Clan Versus Points</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'
				for (var i in resp.items) {
					count++;
					strHtml += '<tr>'+
					'<th scope="row">'+count+'</th>'+
					'<td>'+resp.items[i].tag+'</td>'+
					'<td><img src="'+resp.items[i].badgeUrls.small+'" alt="ClãLogo" width="42" /> '+resp.items[i].name+'</td>'+
					'<td>'+resp.items[i].location.id+'</td>'+
					'<td>'+resp.items[i].location.name+'</td>'+
					'<td>'+resp.items[i].location.isCountry+'</td>'+
					'<td>'+resp.items[i].location.countryCode+'</td>'+
					'<td>'+resp.items[i].clanLevel+'</td>'+
					'<td>'+resp.items[i].members+'</td>'+
					'<td>'+resp.items[i].previousRank+'</td>'+
					'<td>'+resp.items[i].clanVersusPoints+'</td>'+
					'</tr>'
				}
				strHtml += '</tbody>'+
				'</table>'+
				'</div>'+
				'</div>'
				recordSearch('Getclanversusrankingsforaspecificlocation', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			} else if (document.getElementById('SelectOptions').value == "Getplayerversusrankingsforaspecificlocation") {
				var strHtml = '<div class="col-12">'+
				'<div style="position: relative; overflow: auto; width: 100%;">'+
				'<table class="table table-responsive">'+
				'<thead>'+
				'<tr>'+
				'<th>Rank</th>'+
				'<th>Tag</th>'+
				'<th>Nome</th>'+
				'<th>Nível</th>'+
				'<th>Rank Anterior</th>'+
				'<th>Troféus CC</th>'+
				'<th>Clã Tag</th>'+
				'<th>Nome Do Clã</th>'+
				'<th>Batalhas Ganhas CC</th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'
				for (var i in resp.items) {
					count++;
					strHtml += '<tr>'+
					'<th scope="row">'+count+'</th>'+
					'<td>'+resp.items[i].tag+'</td>'+
					'<td>'+resp.items[i].name+'</td>'+
					'<td>'+resp.items[i].expLevel+'</td>'+
					'<td>'+resp.items[i].previousRank+'</td>'+
					'<td>'+resp.items[i].versusTrophies+'</td>'
					try {
						strHtml += '<td>'+resp.items[i].clan.tag+'</td>'+
						'<td><img src="'+resp.items[i].clan.badgeUrls.small+'" alt="ClãLogo" /> '+resp.items[i].clan.name+'</td>'
					} catch (e) {
						strHtml += '<td>Sem Clã</td>'+
						'<td>Sem Clã</td>'
					}
					strHtml += '<td>'+resp.items[i].versusBattleWins+'</td>'+
					'</tr>'
				}
				strHtml += '</tbody>'+
				'</table>'+
				'</div>'+
				'</div>'
				recordSearch('Getplayerversusrankingsforaspecificlocation', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
			}else if (document.getElementById('SelectOptions').value == "Getplayerinformation") {
				var strHtml = '<div class="col-12">'+
				'<ul class="nav nav-tabs" id="myTab" role="tablist">'+
				'<li class="nav-item">'+
				'<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-expanded="true">Meu Perfil</a>'+
				'</li>'+
				'<li class="nav-item">'+
				'<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile">Meu Clã</a>'+
				'</li>'+
				'</ul>'+
				'<div class="tab-content" id="myTabContent">'+
				'<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">'+
				'<ul class="nav nav-tabs" id="myTab" role="tablist">'+
				'<li class="nav-item">'+
				'<a class="nav-link active" style="text-decoration: none;" id="home-tab1" data-toggle="tab" href="#home1" role="tab" aria-controls="home" aria-expanded="true">Vila Principal</a>'+
				'</li>'+
				'<li class="nav-item">'+
				'<a class="nav-link" style="text-decoration: none;" id="profile-tab1" data-toggle="tab" href="#profile1" role="tab" aria-controls="profile">Base Do Construtor</a>'+
				'</li>'+
				'</ul>'+
				'<div class="tab-content" id="myTabContent">'+
				'<div class="tab-pane fade show active" id="home1" role="tabpanel" aria-labelledby="home-tab1">'+
				'<div class="row" style="background-color: #eaeae1">'+
				'<div class="col-6" style="border: 3px solid #71769f; border-top-left-radius: 8px; background-color: #868cac;">'+
				'<div style="float: left;">'+
				'<canvas id="myCanvas" width="70px" height="70px" style="background-image: url("../img/lvl.png"); background-repeat: no-repeat; background-size: 70px 70px;"></canvas>'+
				'</div>'+
				'<div style="float: left; margin-left: 5px; margin-bottom: -7px; font:20px scfont, Arial, Tahoma, Sans-serif;"><div style="color: white;">'+
				resp.name+
				'</div>'+
				'</div><br>'+
				'<div style="color: #545776; font-size: 16px; font-weight: bold; font-family: Times New Roman;">'+
				' '+resp.tag+
				'</div>'
				try{
					strHtml += '<br><br>'+
					'<div style="float: left;">'+
					'<img src="'+resp.clan.badgeUrls.small+'" alt="ClãLogo" />'+
					'</div>'+
					'<div style="float: left; margin-left: 5px; margin-bottom: -6px ">'+
					'<div style="color: white; font:20px scfont, Arial, Tahoma, Sans-serif;">'+
					resp.clan.name+
					'</div>'+
					'</div><br>'+
					'<div style="float: left; color: white; font-size: 16px; font-weight: bold; font-family: Times New Roman;">'+
					 resp.role+
					'</div><br><br><br><br><br><br>'
				}catch(e){
					strHtml += '<br><br><br><br>';
				}
				try{
					strHtml += '</div>'+
					'<div class="col-6" style="border: 3px solid #71769f; border-top-right-radius: 8px; background-color: #71769f;">'+
					'<div style="float: left;">'+
					'<img src="'+resp.league.iconUrls.small+'" alt="LeagueLogo" />'+
					'</div>'+
					'<div style="color: white; font:10px scfont, Arial, Tahoma, Sans-serif;">'+
					'Level Centro Da Vila: '+resp.townHallLevel+' <br>'+
					'ID Da Liga: '+resp.league.id+' <br>'+
					'</div>'+
					'<div style="float: left; background-color: #757579;">'+
					'<div style="color: white; font:15px scfont, Arial, Tahoma, Sans-serif;">'+
					resp.league.name+'<br>'+
					'<div style="background-color: #706580; color: white; font:15px scfont, Arial, Tahoma, Sans-serif;">'+
					resp.trophies+
					'</div>'+
					'</div>'+
					'</div>'+
					'<div><br><br><br>'+
					'<div style="float: left; color: white; font-weight: bold; font-family: Times New Roman; float: left;">'+
					'Estrela De Guerra Ganhas<br>'+
					'<div style="border: 1px solid 706580; background-color: #706580; color: white; font:15px scfont, Arial, Tahoma, Sans-serif;">'+
					resp.warStars+
					'</div>'+
					'</div> '+
					'<div style="float: left;">      </div>'+
					'<div style="float: left; color: white; font-weight: bold; font-family: Times New Roman; float: left;">'+
					'Melhor De Todos Os Tempos<br>'+
					'<div style="border: 1px solid 706580; background-color: #706580; color: white; font:15px scfont, Arial, Tahoma, Sans-serif;">'+
					resp.bestTrophies+
					'</div>'+
					'</div><br><br><br><br>'+
					'</div>'+
					'</div>'
				}catch(e){
					strHtml += '</div>'+
					'<div class="col-6" style="border: 3px solid #71769f; border-top-right-radius: 8px; background-color: #71769f;"></div>'
				}
				strHtml += '<div class="col-12" style="border: 3px solid #6f5f9b; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; background-color: #4e4d79; margin-top: -20px">'+
				'<div style="color: white; font-weight: bold; font-family: Times New Roman; float: left;">'+
				'Tropas Doadas:    '+
				'</div>'+
				'<div style="border-radius: 5px; float: left; background-color: #2e2c62; color: white; font-weight: bold;">'+
				'      '+resp.donations+' '+
				'</div>'+
				'<div style="color: white; font-weight: bold; font-family: Times New Roman; float: left;">'+
				'   Tropas Recebidas:    '+
				'</div>'+
				'<div style="border-radius: 5px; float: left; background-color: #2e2c62; color: white; font-weight: bold;">'+
				'      '+resp.donationsReceived+' '+
				'</div>'+
				'<div style="color: white; font-weight: bold; font-family: Times New Roman; float: left;">'+
				'   Ataques Ganhos:    '+
				'</div>'+
				'<div style="border-radius: 5px; float: left; background-color: #2e2c62; color: white; font-weight: bold;">'+
				'      '+resp.attackWins+' '+
				'</div>'+
				'<div style="color: white; font-weight: bold; font-family: Times New Roman; float: left;">'+
				'   Defesas Vencidas:    '+
				'</div>'+
				'<div style="border-radius: 5px; float: left; background-color: #2e2c62; color: white; font-weight: bold;">'+
				'      '+resp.defenseWins+' '+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="tab-pane fade" id="profile1" role="tabpanel" aria-labelledby="profile-tab1">'+
				'<div class="row" style="background-color: #eaeae1">'+
				'<div class="col-6" style="border: 3px solid #71769f; border-top-left-radius: 8px; background-color: #868cac;">'+
				'<div style="float: left;">'+
				"<canvas id='myCanvas1' width='70px' height='70px' style='background-image: url('../img/lvl.png'); background-repeat: no-repeat; background-size: 70px 70px;'></canvas>"+
				'</div>'+
				'<div style="float: left; margin-left: 5px; margin-bottom: -7px; font:20px scfont, Arial, Tahoma, Sans-serif;"><div style="color: white;">'+
				resp.name+
				'</div>'+
				'</div><br>'+
				'<div style="color: #545776; font-size: 16px; font-weight: bold; font-family: Times New Roman;">'+
				' '+resp.tag+
				'</div>'
				try{
					strHtml += '<br><br>'+
					'<div style="float: left;">'+
					'<img src="'+resp.clan.badgeUrls.small+'" alt="ClãLogo" />'+
					'</div>'+
					'<div style="float: left; margin-left: 5px; margin-bottom: -6px ">'+
					'<div style="color: white; font:20px scfont, Arial, Tahoma, Sans-serif;">'+
					resp.clan.name+
					'</div>'+
					'</div><br>'+
					'<div style="float: left; color: white; font-size: 16px; font-weight: bold; font-family: Times New Roman;">'+
					' '+resp.role+
					'</div><br><br><br><br><br><br>'
				}catch(e){
					strHtml += '<br><br><br><br>';
				}
				strHtml += '</div>'+
				'<div class="col-6" style="border: 3px solid #71769f; border-top-right-radius: 8px; background-color: #71769f;">'+
				'<div style="float: left;">'+
				'<img src="'+resp.league.iconUrls.small+'" alt="LeagueLogo" />'+
				'</div>'+
				'<div style="color: white; font:10px scfont, Arial, Tahoma, Sans-serif;">'+
				'Nivel Casa Do Construtor: '+resp.builderHallLevel+' <br>'+
				'</div>'+
				'<div style="float: left; background-color: #757579;">'+
				'<div style="color: white; font:15px scfont, Arial, Tahoma, Sans-serif;">'+
				'                    <br>'+
				'<div style="background-color: #706580; color: white; font:15px scfont, Arial, Tahoma, Sans-serif;">'+
				'    '+resp.versusTrophies+
				'</div>'+
				'</div>'+
				'</div><br><br><br>'+
				'<div style="float: left; color: white; font-weight: bold; font-family: Times New Roman; float: left;">'+
				'Melhor De Todos Os Tempos<br>'+
				'<div style="border: 1px solid 706580; background-color: #706580; color: white; font:15px scfont, Arial, Tahoma, Sans-serif;">'+
				'     '+resp.bestVersusTrophies+
				'</div>'+
				'</div> '+
				'<br><br><br><br>'+
				'<!-- Nivel Casa Do Construtor: '+resp.builderHallLevel+' <br>'+
				'Troféus Casa Do Construtor: '+resp.versusTrophies+' <br>'+
				'Melhor De Todos Os Tempos: '+resp.bestVersusTrophies+' <br><br><br><br> -->'+
				'</div>'+
				'<div class="col-12" style="border: 3px solid #6f5f9b; border-bottom-left-radius: 8px; border-bottom-right-radius: 8px; background-color: #4e4d79; margin-top: -20px">'+
				'<div style="border-radius: 5px; float: right; background-color: #2e2c62; color: white; font-weight: bold;">'+
				'  '+resp.versusBattleWins+' '+
				'</div>'+
				'<div style="color: white; font-weight: bold; font-family: Times New Roman; float: right;">'+
				'Vitórias Em Batalhas Simultâneas:    '+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="row" style="background-color: #eaeae1">'+
				'</div>'+
				'<div class="row" style="background-color: #eaeae1">'+
				'<div class="col-12">'+
				'<div class="list-group">'
				var index = true;
				for (var i in resp.achievements) {
					count++;
					if (i == 0) {
						strHtml+='<li class="list-group-item">'+
						'<div style="color: white; font:15px scfont, Arial, Tahoma, Sans-serif; text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;">'+
						'Conquistas Da Vila Principal'+
						'</div>'+
						'</li>'
					}
					if (resp.achievements[i].village !== "home" && index) {
						index = false;
						strHtml +='<li class="list-group-item">'+
						'<div style="color: white; font:15px scfont, Arial, Tahoma, Sans-serif; text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;">'+
						'Conquistas Da Base Do Construtor'+
						'</div>'+
						'</li>'
					}
					strHtml +='<div class="list-group-item  list-group-item-secondary flex-column align-items-start">'+
					'<div class="d-flex w-100 justify-content-between">'+
					resp.achievements[i].stars+'/3<h5 class="mb-1">'+resp.achievements[i].name+'</h5>'+
					'<small class="text-muted">'+resp.achievements[i].value+'/'+resp.achievements[i].target+'</small>'+
					'</div>'+
					'<p class="mb-1">'+resp.achievements[i].info+'</p>'+
					'<small class="text-muted" style="float: right;">'+resp.achievements[i].completionInfo+'</small>'+
					'</div>'
				}
				strHtml += '</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">'+
				'<div class="col-8">'
				try{
					strHtml += '<div class="row" style="background-color: #C0C7C0;">'+
					'<div class="col-12" style="border-radius: 8px; background-color: #C0C7C0;">'+
					'<div style="float: left;">'+
					'<img src="'+resp.clan.badgeUrls.small+'" alt="ClãLogo" />'+
					'</div>'+
					'    <span style="color: white; font:15px scfont, Arial, Tahoma, Sans-serif;">'+resp.clan.name+'</span>'+
					'<span style="float: right;">'+resp.clan.tag+'</span><br>'+
					'    Level Do Clã: '+resp.clan.clanLevel+' <br>'+
					'</div>'+
					'</div>'
				}catch(e){
					strHtml += '<br><br><br>';
				}
				strHtml += '</div>'+
				'</div>'+
				'</div>'+
				'</div>'
				recordSearch('Getplayerinformation', strHtml);
				document.getElementById("clanInformations2").innerHTML = strHtml;
				var canvas = document.getElementById('myCanvas');
				var canvas2 = document.getElementById('myCanvas1');
				var context = canvas.getContext('2d');
				var context2 = canvas2.getContext('2d');
				context.font = 'bold 25pt Calibri';
				context.fillStyle = 'white';
				context.fillText(resp.expLevel, 18, 43);
				context2.font = 'bold 25pt Calibri';
				context2.fillStyle = 'white';
				context2.fillText(resp.expLevel, 18, 43);
			}
		});
	return false;
	}