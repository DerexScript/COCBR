<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.4.9
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2009 Invision Power Services
|
|   ========================================
|
|
|
+---------------------------------------------------------------------------
*/

$PRE = trim(ipsRegistry::dbFunctions()->getPrefix());
$DB  = ipsRegistry::DB();


$TABLE	= 'core_share_links_log';
$SQL[]	= "ALTER TABLE core_share_links_log CHANGE log_ip_address log_ip_address VARCHAR( 46 ) NOT NULL;";


