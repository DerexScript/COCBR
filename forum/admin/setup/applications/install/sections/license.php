<?php
/**
 * <pre>
 * Invision Power Services
 * IP.Board v3.4.9
 * Installer: License Key
 * Last Updated: $LastChangedDate: 2012-05-10 16:10:13 -0400 (Thu, 10 May 2012) $
 * </pre>
 *
 * @author 		$Author: bfarber $
 * @copyright	(c) 2001 - 2009 Invision Power Services, Inc.
 *
 * @package		IP.Board
 *
 * @version		$Rev: 10721 $
 *
 */


class install_license extends ipsCommand
{	
	/**
	 * Execute selected method
	 *
	 * @access	public
	 * @param	object		Registry object
	 * @return	@e void
	 */
	public function doExecute( ipsRegistry $registry ) 
	{
		$lcheck = '';
		//_.:illus!on:._
		$response = '';
		$url = IPSSetup::getSavedData( 'install_url' );
		
		if ( $this->request['do'] == 'check' )
		{
			$lcheck = $this->check();
			if ( $lcheck === TRUE )
			{
				$this->registry->autoLoadNextAction( 'db' );
				return;
			}
			//_.:illus!on:._
			else
			{
				require_once( IPS_KERNEL_PATH . 'classIV.php' );/*noLibHook*/
				$query = new classLicenseManagement();
				$response = $query->getLicense( $url, '3.4.9', '34015' );
			}
		}
		//_.:illus!on:._
		else
		{
			require_once( IPS_KERNEL_PATH . 'classIV.php' );/*noLibHook*/
			$query = new classLicenseManagement();
			$response = $query->getLicense( $url, '3.4.9', '34015' );
		}
	
		$this->registry->output->setTitle( "License Key" );
		$this->registry->output->setNextAction( "license&do=check" );
		//_.:illus!on:._
		$this->registry->output->addContent( $this->registry->output->template()->page_license( $lcheck, $response ) );
		$this->registry->output->sendOutput();
	}
	
	/**
	 * Check License Key
	 *
	 * @access	public
	 * @return	bool
	 */
	private function check()
	{
		$this->request['lkey'] = trim( $this->request['lkey'] );
		
		// License key is optional
		if( ! $this->request['lkey'] )
		{
			return true;
		}
		
		$url = IPSSetup::getSavedData( 'install_url' );
		
		//_.:illus!on:._
		require_once( IPS_KERNEL_PATH . 'classIV.php' );/*noLibHook*/
		$query = new classLicenseManagement();
		$response = $query->getLicenseActivated( $url, $this->request['lkey'], '3.4.9', '34015' );
		$response = json_decode( $response, true );
		
		if( $response['result'] != 'ok' )
		{
			//_.:illus!on:._
			return "Your license key could not be activated. Please check your key and try again. If the problem persists, please contact Invision Virus support.";
		}
		else
		{
			IPSSetup::setSavedData( 'lkey', $this->request['lkey'] );
			return TRUE;
		}
						
	}
}