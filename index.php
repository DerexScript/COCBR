<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Bootstrap CSS -->
	<base href="/" />
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<title>Clash Of Clans Dicas, Gemas, Noticias e Layouts</title>
	<script src="https://use.fontawesome.com/870060c86f.js"></script>
	<link rel="icon" href="img/ico.png" />
	<meta property="og:locale" content="pt-BR">
	<meta property="og:title" content="COCBR">
	<meta property="og:description" content="Clash Of Clans & Clash Of Royale Dicas, Consultas de informações sobre o jogo em tempo real,  Noticias, Layouts, et etc..">
	<meta property="og:image" content="img/coclogo.png"/>
	<style>
	.d-block {
		max-height: 400px !important;
	}
	@font-face {
		font-family: scfont;
		src: url('font/Supercell-magic-webfont.ttf');
	}
</style>
</head>
<body>
	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img class="d-block w-100" src="img/slide1.png" alt="First slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="img/slide2.jpg" alt="Second slide">
			</div>
			<div class="carousel-item">
				<img class="d-block w-100" src="img/slide3.jpg" alt="Third slide">
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

	<nav class="navbar sticky-top navbar-expand navbar-light" style="background-color: #E6F48A;">
		<img src="img/coclogo.png" style="max-height: 50px;">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="//<?php echo $_SERVER['HTTP_HOST'];?>">Home<span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="ConsultAPI/">Consult API COC</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="forum/">Forum</a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="container-fluid" style="margin-top: 5px;">
		<div class="row">
			<div class="col-4">
				<nav class="nav flex-column">
					<span class="nav-link" style="font:20px scfont, Arial, Tahoma, Sans-serif;">Navegação</span>
					<a class="nav-link active" href="//<?php echo $_SERVER['HTTP_HOST'];?>">Home</a>
					<a class="nav-link" href="ConsultAPI/">Consult API COC</a>
					<a class="nav-link" href="forum/">Forum</a>
				</nav>
			</div>



			<div class="col-8">
				<div class="jumbotron jumbotron-fluid ">
					<div class="container">
						<h1 class="display-3">Em Construção</h1>
						<p class="lead">Pagina temporariamente indisponívell</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="bg-dark mt-4">
		<div class="container-fluid py-3" style="background-color: #E6F48A;">
			<div class="row">
				<div class="col-md-3">
					<h5>Clash Of Clans & Clash Of Royale</h5></div>
					<div class="col-md-3"></div>
					<div class="col-md-3"></div>
					<div class="col-md-3"></div>
				</div>
				<div class="row">
					<div class="col-md-6"><a href="https://pt.wikipedia.org/wiki/Clash_of_Clans" target="_blank">Clash of Clans - Wikipedia</a><span class="small"><br><a href="https://chat.whatsapp.com/KND2hTCrULcAiCV0muTDow" target="_blank" style="text-decoration: none;"><img src="img/WhatsApp.ico" alt="Grupo WhatsApp" height="32" style="float: left;" width="32"><h6 style="margin-right: 3px; margin-top: 5px;">Clash Of Clans & Royale - Grupo WhatsApp</h6><br></a>&copy; Copyright 2017 - <?php echo date("Y"); ?>, Comunidade "Clash Of Clans" e "Clash Of Royale" Brasil - Todos direitos reservados.</span></div>
					<div class="col-md-3"></div>
					<div class="col-md-3 text-right small align-self-end">Project: <a href="https://github.com/DerexScript/COCBR" target="_blank" class="fa fa-github" aria-hidden="true"> GitHub</a>, <a href="https://gitlab.com/DerexScript/" target="_blank" class="fa fa-gitlab" aria-hidden="true"> GitLab</a></div>
				</div>
			</div>
		</div>
	</footer>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript">
		//setTimeout(()=>{
		//	document.body.lastElementChild.parentNode.removeChild(document.body.lastElementChild);
		//},0)
	</script>
</body>
</html>