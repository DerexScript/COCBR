<?php 
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	require_once('../connection.php');
	if (isset($_POST['optSearch']) && !empty($_POST['optSearch'])){
		if (isset($_POST['valueSearch']) && !empty($_POST['valueSearch'])){
			try {
				$InsertPDO = "INSERT INTO search_clan (optSearch, clan_innerHtml, date_Search, ip_user) VALUES (:optSearch, :clan_innerHtml, :date_Search, :ip_user)";
				$Result = $ConnPDO->prepare($InsertPDO);
				$Result->bindValue(':optSearch', $_POST['optSearch'], PDO::PARAM_STR);
				$Result->bindValue(':clan_innerHtml', $_POST['valueSearch'], PDO::PARAM_STR);
				date_default_timezone_set('America/Recife');
				$Result->bindValue(':date_Search', date("Y-m-d H:i:s"), PDO::PARAM_STR);
				$Result->bindValue(':ip_user', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
				$Result->execute();
				echo "Gravado Com Sucesso!";
			} catch(PDOException $e) {
				echo $e->getCode().$e->getMessage();	
			}
		}
	}
}else{
	header('Location: ../index.php'); 
}
?>