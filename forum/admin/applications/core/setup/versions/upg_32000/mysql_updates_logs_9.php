<?php
/*
+--------------------------------------------------------------------------
|   IP.Board v3.4.9
|   ========================================
|   by Matthew Mecham
|   (c) 2001 - 2009 Invision Power Services
|
|   ========================================
|
|
|
+---------------------------------------------------------------------------
*/

$PRE = trim(ipsRegistry::dbFunctions()->getPrefix());
$DB  = ipsRegistry::DB();


$TABLE	= 'spam_service_log';
$SQL[]	= "ALTER TABLE spam_service_log CHANGE ip_address ip_address VARCHAR( 46 ) NOT NULL;";


