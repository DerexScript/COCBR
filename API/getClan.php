<?php
header("Content-Type: application/json; charset=utf-8");
if (isset($_POST['clanTag']) && isset($_POST['optSearch']) ){
	$clantag = $_POST['clanTag'];
	$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjBhMTg1ZDg4LWI4ZTgtNDA0Ni05NWYzLTIxZDM0MjJlYmY3ZiIsImlhdCI6MTUxMTM4MjQ1MCwic3ViIjoiZGV2ZWxvcGVyLzRmZWYyNTRkLWNmZDktMmY1OS0wY2NlLWZlN2E3OGYzODM2NSIsInNjb3BlcyI6WyJjbGFzaCJdLCJsaW1pdHMiOlt7InRpZXIiOiJkZXZlbG9wZXIvc2lsdmVyIiwidHlwZSI6InRocm90dGxpbmcifSx7ImNpZHJzIjpbIjE4Ny44NC4yMzEuMTU1Il0sInR5cGUiOiJjbGllbnQifV19.PoB7qmJy0U_Et1vVTbzJWJ5gocaNhTKSJstNPys7mZMnVofLHQbWTkgOc0I9lC72p-t-DqXxbFSNX7pcwemP1Q";
	if ($_POST['optSearch'] == "ListClanMembers"){
		$url = "https://api.clashofclans.com/v1/clans/".urlencode('#'.$clantag)."/members";
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "SearchClans" ){
		$url = "https://api.clashofclans.com/v1/clans?name=".urlencode($clantag);
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "GetClanInformation" ){
		$url = "https://api.clashofclans.com/v1/clans/".urlencode('#'.$clantag);
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "Retrieveclansclanwarlog" ){
		$url = "https://api.clashofclans.com/v1/clans/".urlencode($clantag)."/warlog";
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "Retrieveinformationaboutclanscurrentclanwar" ){
		$url = "https://api.clashofclans.com/v1/clans/".urlencode($clantag)."/currentwar";
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "ListLocations" ){
		$url = "https://api.clashofclans.com/v1/locations/";
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "Getlocationinformation" ){
		$url = "https://api.clashofclans.com/v1/locations/".urlencode($clantag);
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "Getclanrankingsforaspecificlocation" ){
		$url = "https://api.clashofclans.com/v1/locations/".urlencode($clantag)."/rankings/clans";
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "Getplayerrankingsforaspecificlocation" ){
		$url = "https://api.clashofclans.com/v1/locations/".urlencode($clantag)."/rankings/players";
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "Getclanversusrankingsforaspecificlocation" ){
		$url = "https://api.clashofclans.com/v1/locations/".urlencode($clantag)."/rankings/clans-versus";
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "Getplayerversusrankingsforaspecificlocation" ){
		$url = "https://api.clashofclans.com/v1/locations/".urlencode($clantag)."/rankings/players-versus";
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}elseif ( $_POST['optSearch'] == "Getplayerinformation" ){
		$url = "https://api.clashofclans.com/v1/players/".urlencode("#".$clantag);
		$ch = curl_init($url);
		$headr = array();
		$headr[] = "Accept: application/json";
		$headr[] = "Authorization: Bearer ".$token;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headr);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$res = curl_exec($ch);
		$data = json_decode($res, true);
		curl_close($ch);
		echo $res;
	}
}
?>